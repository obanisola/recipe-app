//Password- bgkMYgiDOmgf0tI5
//Connection- mongodb+srv://Tobi:<password>@recipes-ttshd.mongodb.net/test?retryWrites=true&w=majority

const Recipe = require('./models/recipe');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

mongoose.connect('mongodb+srv://Tobi:bgkMYgiDOmgf0tI5@recipes-ttshd.mongodb.net/test?retryWrites=true&w=majority')
 .then(() => {
   console.log('Successfully connected to MongoDB Atlas!');
 })
 .catch((error) => {
   console.log('Unable to connect to MongoDB Atlas!');
   console.error(error);
 });


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.post('/api/recipes', (req, res, next) => {
    const thing = new Recipe({
      title: req.body.title,
      ingredients: req.body.ingredients,
      instructions: req.body.instructions,
      difficulty: req.body.difficulty,
      time: req.body.time,
    });
    thing.save().then(
      () => {
       res.status(201).json({
         message: 'Recipe saved successfully!'
       });
      }
    ).catch(
      (error) => {
       res.status(400).json({
         error: error
       });
      }
    );
});

app.get('/api/recipes/:id', (req, res, next) => {
    Recipe.findOne({
      _id: req.params.id
    }).then(
      (recipe) => {
       res.status(200).json(recipe);
      }
    ).catch(
      (error) => {
       res.status(404).json({
         error: error
       });
      }
    );
});

app.put('/api/recipes/:id', (req, res, next) => {
    const recipe = new Recipe({
      _id: req.params.id,
      title: req.body.title,
      ingredients: req.body.ingredients,
      instructions: req.body.instructions,
      difficulty: req.body.difficulty,
      time: req.body.time,
    });
    Recipe.updateOne({_id: req.params.id}, recipe).then(
      () => {
       res.status(201).json({
         message: 'Recipe updated successfully!'
       });
      }
    ).catch(
      (error) => {
       res.status(400).json({
         error: error
       });
      }
    );
});

app.delete('/api/recipes/:id', (req, res, next) => {
    Recipe.deleteOne({_id: req.params.id}).then(
      () => {
       res.status(200).json({
         message: 'Recipe Deleted!'
       });
      }
    ).catch(
      (error) => {
       res.status(400).json({
         error: error
       });
      }
    );
}); 


app.use('/api/recipes', (req, res, next) => {
    Recipe.find().then(
      (recipes) => {
       res.status(200).json(recipes);
      }
    ).catch(
      (error) => {
       res.status(400).json({
         error: error
       });
      }
    );
});


app.use('/api/recipes', (req, res, next) => {
 const recipes = [
   {
    title: 'Banana Nut Bread',
    ingredients: '1 box yellow cake mix, 1 package Jell-O Instant Banana Cream Pudding & Pie Filling, 1/2 cup water, 1/2 cup vegetable oil, 2 ripe bananas, mashed 4 eggs 1 cup chopped pecans',
    instructions: 'Step 1- Heat oven to 350° F. Step 2- Mix all the ingredients well. Pour into 2 large or 4 small greased loaf pans. Step 3- Bake for 40 to 45 minutes. (For those allergic to nuts, this recipe can be made without the pecans.)',
    difficulty: 1,
    time: 50,
    _id: 'obanisolatobi',
    },
    {
    title: 'Orange Nut Cake',
    ingredients: '1 box yellow cake mix, 1 package Jell-O Instant Banana Cream Pudding & Pie Filling, 1/2 cup water, 1/2 cup vegetable oil, 2 ripe bananas, mashed 4 eggs 1 cup chopped pecans',
    instructions: 'Step 1- Heat oven to 350° F. Step 2- Mix all the ingredients well. Pour into 2 large or 4 small greased loaf pans. Step 3- Bake for 40 to 45 minutes. (For those allergic to nuts, this recipe can be made without the pecans.)',
    difficulty: 3,
    time: 70,
    _id: 'obanisoladeborah',
   },
 ];
 res.status(200).json(recipes);
});

module.exports = app;